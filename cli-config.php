<?php
use Doctrine\ORM\Tools\Console\ConsoleRunner;

require_once 'config/doctrine/bootstrap.php';

return ConsoleRunner::createHelperSet($entityManager);