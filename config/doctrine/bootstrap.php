<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$paths = [ __DIR__.'/yml' ];
$isDevMode = true;

$dbParams = [
    'driver'   => 'pdo_mysql',
    'user'     => 'root',
    'password' => 'devdb',
    'dbname'   => 'devdb',
    'host' => '172.21.0.2',
    'port' => 3306
];

$config = Setup::createYAMLMetadataConfiguration($paths, $isDevMode);
$entityManager = EntityManager::create($dbParams, $config);

$app['entity.manager'] = function() use ($entityManager) {
    return $entityManager;
};
