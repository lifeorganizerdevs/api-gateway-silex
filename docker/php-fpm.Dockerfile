FROM php:7-fpm
 
RUN apt-get update && buildDeps="libpq-dev libzip-dev" \
    && apt-get install -y $buildDeps git  nano wget --no-install-recommends \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug \
    && docker-php-ext-install pdo pdo_mysql zip bcmath

COPY xdebug.ini /usr/local/etc/php/conf.d/
 
WORKDIR /var/www/app