<?php 
namespace Controllers;

use DTOs\RequestMetadata;
use Services\JWT\InvalidTokenException;
use Services\JWT\JWT;
use Services\JWT\MissedAuthenticationHeaderException;
use Services\PathMatcher\EndpointsMatcher;
use Endpoints\Ports\EndpointCaller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouteCollection;

final class ApiGatewayController
{
    private $routes;
    private $endpoints_matcher;
    private $endpoints_caller;
    private $token_manager;

    public function __construct(
        RouteCollection $routes,
        EndpointsMatcher $endpoints_matcher,
        EndpointCaller $endpoints_caller,
        JWT $token_manager
    ) {
        $this->routes = $routes;
        $this->endpoints_matcher = $endpoints_matcher;
        $this->endpoints_caller = $endpoints_caller;
        $this->token_manager = $token_manager;
    }

    public function dispatchRequest(Request $request)
    {
        try {
            $this->validRequest($request);
        } catch (\Exception $e) {
            return (new JsonResponse())->setData([ 'invalid_token' => true ]);
        }

        $request_metadata = $this->createRequestMetadata($request);
        $request_metadata->setRequestUserId($this->getUserId($this->getToken($request)));
        $endpoints = $this->endpoints_matcher->getEndpoints($request_metadata);
        $merged_requests = $this->endpoints_caller->dispatchRequest($request_metadata, $endpoints);

        //TODO: remove it
        $merged_requests['user_id'] = $this->getUserId($this->getToken($request));
        return (new JsonResponse())->setData($merged_requests);
    }

//    public function registerRoute(Request $request)
//    {
//        return (new JsonResponse())->setStatusCode(200);
//    }

    private function validRequest(Request $request) : void
    {
        $token = $this->getToken($request);

        if (!$this->token_manager->validate($token)) {
            throw new InvalidTokenException();
        }
    }

    private function getUserId(string $token) : int
    {
        return $this->token_manager->getUserId($token);
    }

    private function getToken(Request $request) : string
    {
        $token = $request->headers->get('Authorization');
        if(is_null($token)) {
            throw new MissedAuthenticationHeaderException();
        } else {
            return $token;
        }
    }

    private function createRequestMetadata(Request $request) : RequestMetadata 
    {
        return new RequestMetadata(
            $request->get('_route'),
            $request->getMethod(),
            $request->get('_route_params'),
            $request->query->all(),
            $request->request->all(),
            $request->getQueryString()
        );
    }
}
