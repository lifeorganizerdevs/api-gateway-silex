<?php 
namespace Controllers;

use DTOs\NewUser;
use DTOs\TokenDTO;
use Services\Auth\AuthenticationFactory;
use Services\JWT\JWT;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

final class AuthController
{
    private $authenticationFactory;
    private $jwt;

    public function __construct(
        AuthenticationFactory $authenticationFactory,
        JWT $jwt
    ) {
        $this->authenticationFactory = $authenticationFactory;
        $this->jwt = $jwt;
    }

    public function login(Request $request)
    {
        $token_dto = $this->createTokenDTO($request);

        $token = $this->jwt->createToken($token_dto);

        $response = new JsonResponse();
        $response->setData([
            "token" => $token,
            "valid" => $this->jwt->validate($token)
        ]);

        return $response;
    }

    public function logout(Request $request)
    {
        // required??
    }

    public function register(Request $request)
    {
        $new_user = new NewUser(
            $request->get('login'),
            $request->get('email'),
            $request->get('password')
        );

        $register_service = $this->authenticationFactory->registerFrom('rest');
        $register_service->register($new_user);
    }

    private function createTokenDTO(Request $request) : TokenDTO
    {
        $token_dto = new TokenDTO();
        $token_dto->issuer = 'react-app';
        $token_dto->audience = 'hhh';
        $token_dto->id = 'jhgasf324h';
        $token_dto->issued_at_time = time();
        $token_dto->expiration = time() + 3600;
        $token_dto->user_id = 1;

        return $token_dto;
    }
}
