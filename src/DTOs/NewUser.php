<?php 
namespace DTOs;

final class NewUser
{
    private $email;
    private $login = '';
    private $password = '';

    public function __construct(string $email)
    {
        $this->email = $email;
    }

    public function setEmail(string $email) : void
    {
        $this->email = $email;
    }

    public function setPassword(string $password) : void
    {
        $this->password = $password;
    }

    public function asArray() : array
    {
        return [
            'password' => $this->password,
            'login' => $this->login,
            'email' => $this->email,
        ];
    }
}
