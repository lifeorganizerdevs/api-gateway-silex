<?php 
namespace DTOs;

final class RequestMetadata
{
    private $route_name;
    private $method;
    private $route_parameters;
    private $get_parameters;
    private $post_parameters;
    private $query_string;
    private $user_id = null;

    public function __construct(
        string $route_name,
        string $method,
        array $route_parameters,
        array $get_parameters,
        array $post_parameters,
        string $query_string = null
    ) {
        $this->route_name = $route_name;
        $this->method = $method;
        $this->route_parameters = $route_parameters;
        $this->get_parameters = $get_parameters;
        $this->post_parameters = $post_parameters;
        $this->query_string = $query_string;
    }

    public function getRouteName() : string
    {
        return $this->route_name;
    }

    public function getMethod() : string
    {
        return $this->method;
    }

    public function getRouteParameters() : array
    {
        return $this->route_parameters;
    }

    public function getRouteParameter(string $name)
    {
        return $this->route_parameters[$name];
    }

    public function getGetParameters() : array
    {
        return $this->get_parameters;
    }

    public function getPostParameters() : array
    {
        return $this->post_parameters;
    }

    public function getQueryString() : string
    {
        if (is_null($this->query_string)) {
            return '';
        }

        return $this->query_string;
    }

    public function getRequestUserId() : ?int
    {
        return $this->user_id;
    }

    public function setRequestUserId(int $user_id) : void
    {
        $this->user_id = $user_id;
    }
}
