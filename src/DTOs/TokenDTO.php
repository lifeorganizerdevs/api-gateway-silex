<?php 
namespace DTOs;

final class TokenDTO
{
    public $issuer;
    public $audience;
    public $issued_at_time;
    public $expiration;
    public $query_string_hash;
    public $subject;
    public $context;
    public $id;
    public $user_id;
}
