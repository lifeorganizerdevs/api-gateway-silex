<?php
namespace Endpoints\Adapters;

use DTOs\RequestMetadata;
use Endpoints\Ports\Endpoint;
use Endpoints\Ports\UrlResolver;

final class BaseUrlResolver implements UrlResolver
{
    public function getPath(Endpoint $endpoint, RequestMetadata $request_metadata) : string
    {
        if($endpoint->quantityOfRouteParameters() > 0) {
            return $this->prepareUrl($endpoint, $request_metadata);
        } else {
            return $endpoint->getPath();
        }
    }

    private function prepareUrl(Endpoint $endpoint, RequestMetadata $request_metadata) : string
    {
        $url = stripcslashes($endpoint->getPath());
        foreach ($endpoint->getRouteParameters() as $parameter)
            $url = str_replace("$<$parameter>", $request_metadata->getRouteParameter($parameter), $url);

        return $url . '?' . $request_metadata->getQueryString();
    }
}
