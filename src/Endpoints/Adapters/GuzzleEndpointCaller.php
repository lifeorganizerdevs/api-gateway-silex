<?php
namespace Endpoints\Adapters;

use DTOs\RequestMetadata;
use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use Endpoints\Ports\EndpointCaller;
use Endpoints\EndpointCollection;

final class GuzzleEndpointCaller implements EndpointCaller
{
    private $client;
    private $response_merger;
    private $url_resolver;

    public function __construct()
    {
        $this->response_merger = new GuzzleResponseMerger();
        $this->url_resolver = new BaseUrlResolver();
    }

    public function dispatchRequest(RequestMetadata $request_metadata, EndpointCollection $endpoints) : array
    {
        $this->client = $this->createProperClient($endpoints);
        $requests_to_dispatch = $this->getRequestsToDispatch($request_metadata, $endpoints);
        $pool = $this->createPool($requests_to_dispatch);
        $promise = $pool->promise();
        $promise->wait();

        return $this->response_merger->get();
    }

    private function createProperClient(EndpointCollection $endpoints) : Client
    {
        $client = new Client([
            'timeout' => $endpoints->getMaxTimeout(),
            'connection_timeout' => $endpoints->getMaxConnectionTimeout()
        ]);

        return $client;
    }

    private function getRequestsToDispatch(RequestMetadata $request_metadata, EndpointCollection $endpoints) : array
    {
        $requests = [];
        foreach($endpoints as $endpoint) {
            $path = $this->url_resolver->getPath($endpoint, $request_metadata);

            $requests[] = new Request($endpoint->getMethod(), $path);
        }

        return $requests;
    }

    private function createPool(array $requests_to_dispatch) : Pool
    {
        $response_merger = $this->response_merger;
        $pool = new Pool($this->client, $requests_to_dispatch, [
            'concurrency' => 5,
            'fulfilled' => function ($response, $index) use ($response_merger) {
                $response_merger->fulfilled($response);
            },
            'rejected' => function ($reason, $index) use ($response_merger) {
                $response_merger->error($reason);
            }
        ]);

        return $pool;
    }
}
