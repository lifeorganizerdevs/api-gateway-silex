<?php
namespace Endpoints\Adapters;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ServerException ;
use GuzzleHttp\Psr7\Response;

final class GuzzleResponseMerger
{
    private $response_array;

    public function __construct()
    {
        $this->response_array = [];
    }

    public function fulfilled(Response $response) : void
    {
        $json = json_decode($response->getBody()->getContents());

        $this->response_array[] = (array)$json;
    }

    public function error($response_exception) : void
    {
        if ($response_exception instanceof ConnectException) {
            $this->handleConnectException($response_exception);
        } elseif ($response_exception instanceof ClientException) {
            $this->handleClientException($response_exception);
        } elseif ($response_exception instanceof ServerException) {
            $this->handleServerException($response_exception);
        }
    }

    private function handleConnectException(ConnectException $response) : void
    {
        $this->response_array[] = [
            'connection' => $response->getCode(),
            'message' => $response->getMessage()
        ];
    }

    private function handleClientException(ClientException $response) : void
    {
        $this->response_array[] = [
            'client' => $response->getCode(),
            'message' => $response->getMessage()
        ];
    }

    private function handleServerException(ServerException $response) : void
    {
        $this->response_array[] = [
            'server' => $response->getCode(),
            'message' => $response->getMessage()
        ];
    }

    public function get() : array
    {
        return $this->response_array;
    }
}