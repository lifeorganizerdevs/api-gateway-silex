<?php
namespace Endpoints;

use Endpoints\Ports\Endpoint;
use Endpoints\Exceptions\KeyExistException;
use Endpoints\Exceptions\UnexistingEndpointException;

final class EndpointCollection implements \Iterator, \Countable
{
    private $endpoints_container;
    private $position;

    public function __construct()
    {
        $this->endpoints_container = [];
        $this->position = 0;
    }

    public function current() : Endpoint
    {
        return $this->endpoints_container[$this->position];
    }

    public function key() : int
    {
        return $this->position;
    }

    public function next() : void
    {
        ++$this->position;
    }

    public function rewind() : void
    {
        $this->position = 0;
    }

    public function valid() : bool
    {
        return isset($this->endpoints_container[$this->position]);
    }

    public function count() : int
    {
        return count($this->endpoints_container);
    }

    public function first() : Endpoint
    {
        return reset($this->endpoints_container);
    }

    public function last() : Endpoint
    {
        return end($this->endpoints_container);
    }

    public function add(Endpoint $endpoint) : void
    {
        $this->endpoints_container[] = $endpoint;
    }

    public function addWithKey(int $key, Endpoint $endpoint) : void
    {
        try {
            $this->checkKeyExist($key);
            throw new KeyExistException();
        } catch (\InvalidArgumentException $e) {
            $this->endpoints_container[$key] = $endpoint;
        }
    }

    public function replace(Endpoint $endpoint) : void
    {
        $endpoint_key = $this->getEndpointKeyById($endpoint->getId());
        $this->endpoints_container[$endpoint_key] = $endpoint;
    }

    public function replaceWithKey(int $key, Endpoint $endpoint) : void
    {
        $this->checkKeyExist($key);
        $this->endpoints_container[$key] = $endpoint;
    }

    public function remove(int $key) : void
    {
        $this->checkKeyExist($key);
        unset($this->endpoints_container[$key]);
    }

    public function getByKey(int $key) : Endpoint
    {
        $this->checkKeyExist($key);
        return $this->endpoints_container[$key];
    }

    public function getById(int $id) : Endpoint
    {
        foreach ($this->endpoints_container as $endpoint) {
            if ($endpoint->getId() == $id) {
                return $endpoint;
            }
        }
        throw new UnexistingEndpointException();
    }

    public function getByRouteName(string $route_name) : array
    {
        $endpoints = [];
        foreach ($this->endpoints_container as $endpoint) {
            if ($endpoint->getRouteName() == $route_name) {
                $endpoints[] = $endpoint;
            }
        }
        return $endpoints;
    }

    public function getPaths() : array
    {
        $paths = [];
        foreach ($this->endpoints_container as $endpoint) {
            $paths[] = $endpoint->getPath();
        }

        return $paths;
    }

    public function getMaxTimeout() : float
    {
        $timeout = 0.0;
        foreach ($this->endpoints_container as $endpoint) {
            $endpoint_timeout = $endpoint->getTimeout();
            if ($endpoint_timeout > $timeout) {
                $timeout = $endpoint_timeout;
            }
        }

        return $timeout;
    }

    public function getMaxConnectionTimeout() : float
    {
        $connection_timeout = 0.0;
        foreach ($this->endpoints_container as $endpoint) {
            $endpoint_connection_timeout = $endpoint->getConnectionTimeout();
            if ($endpoint_connection_timeout > $connection_timeout) {
                $connection_timeout = $endpoint_connection_timeout;
            }
        }

        return $connection_timeout;
    }

    private function checkKeyExist(int $key)
    {
        if (!array_key_exists($key, $this->endpoints_container)) {
            throw new \InvalidArgumentException('Key: ' . $key . ' isn\'t exist in collection.');
        }
    }

    private function getEndpointKeyById(int $id) : int
    {
        foreach ($this->endpoints_container as $key => $endpoint) {
            if ($endpoint->getId() == $id) {
                return $key;
            }
        }
        throw new UnexistingEndpointException();
    }
}
