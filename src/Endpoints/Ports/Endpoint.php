<?php
namespace Endpoints\Ports;

interface Endpoint
{
    public function getPath() : string;
    public function getRouteParameters() : array;
    public function quantityOfRouteParameters() : int;
    public function getTimeout() : float;
    public function getConnectionTimeout() : float;
    public function getMethod() : string;
}
