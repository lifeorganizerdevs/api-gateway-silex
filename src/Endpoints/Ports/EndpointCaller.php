<?php
namespace Endpoints\Ports;

use DTOs\RequestMetadata;
use Endpoints\EndpointCollection;

interface EndpointCaller
{
    public function dispatchRequest(RequestMetadata $request_metadata, EndpointCollection $endpoints) : array;
}
