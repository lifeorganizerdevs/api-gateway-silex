<?php
namespace Endpoints\Ports;

use DTOs\RequestMetadata;

interface UrlResolver
{
    public function getPath(Endpoint $endpoint, RequestMetadata $request_metadata) : string;
}