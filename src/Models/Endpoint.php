<?php
namespace Models;

use Endpoints\Ports\Endpoint as EndpointInterface;

class Endpoint implements EndpointInterface
{
    private $id;
    private $route_name;
    private $path;
    private $route_parameters;
    private $timeout;
    private $connection_timeout;
    private $method = 'GET';

    public function getId()
    {
        return $this->id;
    }

    public function setRouteName($routeName)
    {
        $this->route_name = $routeName;

        return $this;
    }

    public function getRouteName()
    {
        return $this->route_name;
    }

    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    public function getPath() : string
    {
        return $this->path;
    }

    public function setRouteParameters($routeParameters)
    {
        $this->route_parameters = $routeParameters;

        return $this;
    }

    public function getRouteParameters() : array
    {
        return $this->route_parameters;
    }

    public function quantityOfRouteParameters() : int
    {
        return count($this->route_parameters);
    }

    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;

        return $this;
    }

    public function getTimeout() : float
    {
        return $this->timeout;
    }

    public function setConnectionTimeout($connectionTimeout)
    {
        $this->connection_timeout = $connectionTimeout;

        return $this;
    }

    public function getConnectionTimeout() : float
    {
        return $this->connection_timeout;
    }

    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    public function getMethod() : string
    {
        return $this->method;
    }
}
