<?php
namespace Models;

use Models\User;

final class Profile
{
    private $id;
    private $user_id;
    private $user;

    public function getId()
    {
        return $this->id;
    }

    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }
}
