<?php
namespace Models;

use Services\Router\Route as RouteInterface;

class Route implements RouteInterface
{
    private $id;
    private $route_name;
    private $host;
    private $pattern;
    private $protocol;
    private $method;

    public function getId() {
        return $this->id;
    }

    public function setRouteName($routeName) {
        $this->route_name = $routeName;

        return $this;
    }

    public function getRouteName() {
        return $this->route_name;
    }

    public function setHost($host) {
        $this->host = $host;

        return $this;
    }

    public function getHost() {
        return $this->host;
    }

    public function setPattern($pattern) {
        $this->pattern = $pattern;

        return $this;
    }

    public function getPattern() {
        return $this->pattern;
    }

    public function setProtocol($protocol) {
        $this->protocol = $protocol;

        return $this;
    }

    public function getProtocol() {
        return $this->protocol;
    }

    public function setMethod($method) {
        $this->method = $method;

        return $this;
    }

    public function getMethod() {
        return $this->method;
    }
}
