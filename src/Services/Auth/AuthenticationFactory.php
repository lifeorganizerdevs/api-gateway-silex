<?php
namespace Services\Auth;

use Services\Auth\Exceptions\InvalidLoginServiceName;
use Services\Auth\Exceptions\InvalidRegistrationServiceName;
use Services\Auth\Ports\Login;
use Services\Auth\Ports\Registration;
use Services\Auth\Adapters\RestLogin;
use Services\Auth\Adapters\RestRegistration;

final class AuthenticationFactory
{
    public function loginWith(string $service_name) : Login
    {
        switch($service_name) {
            case 'rest':
                return $this->restApiLogin();
        }
        throw new InvalidLoginServiceName();
    }

    public function registerFrom(string $service_name) : Registration
    {
        switch($service_name) {
            case 'rest':
                return $this->restApiRegistration();
        }
        throw new InvalidRegistrationServiceName();
    }

    private function restApiLogin() : Login
    {
        return new RestLogin();
    }

    private function restApiRegistration() : Registration
    {
        return new RestRegistration();
    }
}
