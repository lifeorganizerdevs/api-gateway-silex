<?php
namespace Services\JWT;

use DTOs\TokenDTO;

interface JWT
{
    public function validate(string $token) : bool;
    public function createToken(TokenDTO $token_dto) : string;
    public function getUserId(string $token) : int;
}
