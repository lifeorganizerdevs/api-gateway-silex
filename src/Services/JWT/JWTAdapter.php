<?php
namespace Services\JWT;

use DTOs\TokenDTO;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Token;
use Lcobucci\JWT\ValidationData;

final class JWTAdapter implements JWT
{
    private $builder;
    private $parser;
    private $validator_data;
    private $signer;

    public function __construct()
    {
        $this->builder = new Builder();
        $this->parser = new Parser();
        $this->validator_data = new ValidationData();
        $this->signer = new Sha256();
    }

    public function validate(string $token_str) : bool
    {
        $token = $this->parser->parse($token_str);
        $this->setValidatorData($token);

        return $this->tokenIsValidAndVerified($token);
    }

    public function getUserId(string $token_str) : int
    {
        $token = $this->parser->parse($token_str);

        return $token->getClaim('user_id');
    }

    public function createToken(TokenDTO $token_dto) : string
    {
        $token = $this->builder
            ->setIssuer($token_dto->issuer)
            ->setAudience($token_dto->audience)
            ->setId($token_dto->id, true)
            ->setIssuedAt($token_dto->issued_at_time)
            ->setExpiration($token_dto->expiration)
            ->set('user_id', $token_dto->user_id)
            ->sign($this->signer, 'testing');

        return $token->getToken();
    }

    private  function setValidatorData(Token $token) : void
    {
        $this->validator_data->setAudience($token->getClaim('aud'));
        $this->validator_data->setIssuer($token->getClaim('iss'));
        $this->validator_data->setId($token->getClaim('jti'));
    }

    private function tokenIsValidAndVerified(Token $token) : bool
    {
        return $token->validate($this->validator_data) && $token->verify($this->signer, 'testing');
    }
}
