<?php
namespace Services\PathMatcher;

use Doctrine\ORM\EntityManagerInterface;
use DTOs\RequestMetadata;
use Endpoints\EndpointCollection;
use Models\Endpoint;

final class DoctrineEndpointsMatcher implements EndpointsMatcher
{
    private $entity_manager;
    private $endpoints;

    public function __construct(EntityManagerInterface $entity_manager)
    {
        $this->entity_manager = $entity_manager;
        $this->endpoints = new EndpointCollection();
    }

    public function getEndpoints(RequestMetadata $request_metadata) : EndpointCollection
    {
        $repository = $this->entity_manager->getRepository(Endpoint::class);
        $endpoints_from_db = $repository->findBy([ 'route_name' => $request_metadata->getRouteName() ]);
        $this->buildEndpointsCollection($endpoints_from_db);

        return $this->endpoints;
    }

    private function buildEndpointsCollection($endpoints_from_db)
    {
        foreach($endpoints_from_db as $endpoint_db) {
            $this->endpoints->add($endpoint_db);
        }
    }
}
