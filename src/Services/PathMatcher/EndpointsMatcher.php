<?php
namespace Services\PathMatcher;

use DTOs\RequestMetadata;

interface EndpointsMatcher
{
    public function getEndpoints(RequestMetadata $request_metadata);
}
