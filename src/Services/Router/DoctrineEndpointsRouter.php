<?php
namespace Services\Router;

use Doctrine\Common\Persistence\ObjectRepository;
use Silex\Application;

class DoctrineEndpointsRouter implements EndpointsRouter
{
    private $route_repository;

    public function __construct(ObjectRepository $repository)
    {
        $this->route_repository = $repository;
    }

    public function generateMicroservicesRoutes(Application $app): void
    {
        $routes = $this->route_repository->findAll();
        foreach ($routes as $route) {
            $app->{$route->getMethod()}(
                $route->getPattern(),
                'apigateway.controller:dispatchRequest'
            )->bind($route->getRouteName());
        }
    }
}
