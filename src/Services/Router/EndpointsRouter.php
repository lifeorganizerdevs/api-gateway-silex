<?php
namespace Services\Router;

use Silex\Application;

interface EndpointsRouter
{
    public function generateMicroservicesRoutes(Application $app) : void;
}
