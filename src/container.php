<?php

use Endpoints\Adapters\GuzzleEndpointCaller;
use Services\JWT\JWTAdapter;
use Services\PathMatcher\DoctrineEndpointsMatcher;
use Controllers\ApiGatewayController;
use Controllers\AuthController;
use Services\Auth\AuthenticationFactory;
use Services\Router\DoctrineEndpointsRouter;
use Models\Route;

$app['jwt'] = function() {
    return new JWTAdapter();
};

$app['endpoint.caller'] = function() {
    return new GuzzleEndpointCaller();
};

$app['endpoints.router'] = function() use ($app) {
    return new DoctrineEndpointsRouter($app['entity.manager']->getRepository(Route::class));
};

$app['auth.controller'] = function() use ($app) {
    return new AuthController(new AuthenticationFactory(), $app['jwt']);
};

$app['apigateway.controller'] = function() use ($app) {
    return new ApiGatewayController(
        $app['routes'],
        new DoctrineEndpointsMatcher($app['entity.manager']),
        $app['endpoint.caller'],
        $app['jwt']
    );
};
