<?php

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

// TODO: rozwiązanie w fazie tymczasowej do czasu określenia formatu JSON
$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }
    return (new JsonResponse())->setData([ 'error' => $e->getMessage() ])->setStatusCode($code);
});

// tymczasowa ścieżka zwracająca błąd klienta, nie curl dla testów obsługi błędów klienta
$app->get('/exception', function() use ($app) {
    return (new JsonResponse())->setData([ 'error' => 'Unhandled exception.' ])->setStatusCode(404);
});
$app->get('/timeout', function() use ($app) {
    sleep(5);
    return (new JsonResponse())->setData([ 'error' => 'My timeout exception.' ])->setStatusCode(504);
});
$app->post('/login', 'auth.controller:login')
    ->bind('login');
$app->post('/logout', 'auth.controller:logout')
    ->bind('logout');
$app->post('/register', 'auth.controller:register')
    ->bind('register');

$app->post('/route/register', 'apigateway.controller:registerRoute')
    ->bind('route.register');

$app['endpoints.router']->generateMicroservicesRoutes($app);
