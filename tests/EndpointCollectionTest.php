<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Endpoints\EndpointCollection;
use Models\Endpoint;
use Endpoints\Exceptions\KeyExistException;
use Endpoints\Exceptions\UnexistingEndpointException;

final class EndpointCollectionTest extends TestCase
{
    const ROUTE_NAME = 'some.route.name';

    protected $endpoints_collection;
    protected $endpoints_mocks;
    protected $endpoint_quantity;

    public function __construct()
    {
        parent::__construct();
        $endpoint_1 = $this->createMock(Endpoint::class);
        $endpoint_1->method('getId')->will($this->returnValue(1));
        $endpoint_1->method('getRouteName')->will($this->returnValue(EndpointCollectionTest::ROUTE_NAME));
        $endpoint_1->method('getTimeout')->will($this->returnValue(1.2));
        $endpoint_1->method('getConnectionTimeout')->will($this->returnValue(0.9));

        $endpoint_2 = $this->createMock(Endpoint::class);
        $endpoint_2->method('getId')->will($this->returnValue(2));
        $endpoint_2->method('getRouteName')->will($this->returnValue(EndpointCollectionTest::ROUTE_NAME));
        $endpoint_2->method('getTimeout')->will($this->returnValue(2.1));
        $endpoint_2->method('getConnectionTimeout')->will($this->returnValue(1.1));

        $endpoint_3 = $this->createMock(Endpoint::class);
        $endpoint_3->method('getId')->will($this->returnValue(3));
        $endpoint_3->method('getRouteName')->will($this->returnValue(EndpointCollectionTest::ROUTE_NAME));
        $endpoint_3->method('getTimeout')->will($this->returnValue(2));
        $endpoint_3->method('getConnectionTimeout')->will($this->returnValue(1));

        $this->endpoints_mocks = [ $endpoint_1, $endpoint_2, $endpoint_3 ];
    }

    protected function setUp()
    {
        $this->endpoints_collection = new EndpointCollection();

        $this->endpoints_collection->add($this->endpoints_mocks[0]);
        $this->endpoints_collection->add($this->endpoints_mocks[1]);
        $this->endpoints_collection->add($this->endpoints_mocks[2]);     
    }

    public function test_add_proper_endpoint_type() : void
    {
        $endpoint = $this->createMock(Endpoint::class);
        $this->endpoints_collection->add($endpoint);

        $this->assertEquals(4, count($this->endpoints_collection));
    }

    public function test_add_wrong_endpoint_type() : void
    {
        $invalid_endpoint = 'endpoint';

        $this->expectException(\TypeError::class);
        $this->endpoints_collection->add($invalid_endpoint);
    }

    public function test_remove_endpoint_by_proper_key() : void
    {
        $endpoint_key = 0;
        $this->endpoints_collection->remove($endpoint_key);

        $this->assertEquals(2, count($this->endpoints_collection));
    }

    public function test_remove_endpoint_by_wrong_key_type() : void
    {
        $endpoint_key = '1';

        $this->expectException(\TypeError::class);
        $this->endpoints_collection->remove($endpoint_key);
    }

    public function test_remove_endpoint_by_wrong_key() : void
    {
        $endpoint_key = 4;

        $this->expectException(\InvalidArgumentException::class);
        $this->endpoints_collection->remove($endpoint_key);
    }

    public function test_get_removed_endpoint() : void
    {
        $endpoint_key = 2;
        $this->endpoints_collection->remove($endpoint_key);

        $this->expectException(\InvalidArgumentException::class);
        $this->endpoints_collection->getByKey($endpoint_key);
    }

    public function test_replace_endpoint_with_key() : void
    {
        $path = 'asd';
        $endpoint_key = 1;
        $endpoint = $this->createMock(Endpoint::class);
        $endpoint->method('getPath')->will($this->returnValue($path));        
        
        $this->endpoints_collection->replaceWithKey($endpoint_key, $endpoint);

        $this->assertEquals(
            $path,
            $this->endpoints_collection->getByKey($endpoint_key)->getPath()
        );
    }

    public function test_replace_endpoint_with_unexisting_key() : void
    {
        $path = 'asd';
        $endpoint_key = 5;
        $endpoint = $this->createMock(Endpoint::class);
        $endpoint->method('getPath')->will($this->returnValue($path));

        $this->expectException(\InvalidArgumentException::class);
        $this->endpoints_collection->replaceWithKey($endpoint_key, $endpoint);
    }

    public function test_replace_endpoint() : void
    {
        $path = 'qweqwe';
        $endpoint_id = 1;
        $endpoint = $this->createMock(Endpoint::class);
        $endpoint->method('getId')->will($this->returnValue($endpoint_id));
        $endpoint->method('getPath')->will($this->returnValue($path));
        
        $this->endpoints_collection->replace($endpoint);

        $this->assertEquals(
            $path,
            $this->endpoints_collection->getById($endpoint_id)->getPath()
        );
    }

    public function test_replace_unexisting_endpoint() : void
    {
        $path = 'qweqwe';
        $endpoint_id = 10;
        $endpoint = $this->createMock(Endpoint::class);
        $endpoint->method('getId')->will($this->returnValue($endpoint_id));
        $endpoint->method('getPath')->will($this->returnValue($path));
        
        $this->expectException(UnexistingEndpointException::class);
        $this->endpoints_collection->replace($endpoint);
    }

    public function test_add_endpoint_with_unexisting_key() : void
    {
        $path = 'asd';
        $endpoint = new Endpoint();
        $endpoint->setPath($path);
        $endpoint_key = 5;

        $this->endpoints_collection->addWithKey($endpoint_key, $endpoint);

        $this->assertEquals(
            $path,
            $this->endpoints_collection->getByKey($endpoint_key)->getPath()
        );
    }

    public function test_add_endpoint_with_existing_key() : void
    {
        $path = 'asd';
        $endpoint = new Endpoint();
        $endpoint->setPath($path);
        $endpoint_key = 1;

        $this->expectException(KeyExistException::class);
        $this->endpoints_collection->addWithKey($endpoint_key, $endpoint);
    }

    public function test_add_endpoint_without_key() : void
    {
        $path = 'asd';
        $endpoint = new Endpoint();
        $endpoint->setPath($path);

        $this->endpoints_collection->add($endpoint);

        $this->assertEquals(
            $path,
            $this->endpoints_collection->last()->getPath()
        );
    }

    public function test_get_endpoints_by_route_name() : void
    {
        $endpoints_found = $this->endpoints_collection->getByRouteName(EndpointCollectionTest::ROUTE_NAME);

        $this->assertEquals(
            count($this->endpoints_collection),
            count($endpoints_found)
        );
    }

    public function test_get_max_timeout() : void
    {
        $this->assertEquals(
            2.1,
            $this->endpoints_collection->getMaxTimeout()
        );
    }

    public function test_get_max_connection_timeout() : void
    {
        $this->assertEquals(
            1.1,
            $this->endpoints_collection->getMaxConnectionTimeout()
        );
    }
}
